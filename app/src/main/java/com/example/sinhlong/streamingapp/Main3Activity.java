package com.example.sinhlong.streamingapp;

import android.app.VoiceInteractor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Main3Activity extends AppCompatActivity {
    TextView textView;
    ImageView imgView;
    ListView listView;

    String url_vn = "http://192.168.35.108:8080/android/getdata_vn.php";
    String url_us = "http://192.168.35.108:8080/android/getdata_us.php";
    String url_kpop = "http://192.168.35.108:8080/android/getdata_kpop.php";
    String url_top100 = "http://192.168.35.108:8080/android/getdata.php";
    ListViewAdapter adapter;

    ArrayList<Song> list = new ArrayList<>();
    //int []img = {R.drawable.khoi,R.drawable.osad,R.drawable.jayki,R.drawable.alan};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        connectView();
        int position = getIntent().getIntExtra("vitri",0);
        int image = getIntent().getIntExtra("hinhanh",0);
        imgView.setImageResource(image);

        adapter = new ListViewAdapter(Main3Activity.this,list);
        listView.setAdapter(adapter);

        switch (position)
        {
            case 0:
                textView.setText("Những bản nhạc Việt Nam hay nhất");
                //list.clear();
                getData(url_vn);
                break;
            case 1:
                textView.setText("Những bản nhạc US-UK hay nhất");
                //list.clear();
                getData(url_us);
                break;
            case 2:
                textView.setText("Những bản nhạc KPOP hay nhất");
                //list.clear();
                getData(url_kpop);
                break;
            case 3:
                textView.setText("TOP 100 bài hát nghe nhiều nhất");
                //list.clear();
                getData(url_top100);
                break;
            default:
        }

        adapter.notifyDataSetChanged();
    }

    void connectView()
    {
        textView = (TextView) findViewById(R.id.txt_title);
        imgView = (ImageView) findViewById(R.id.img_detail);
        listView = (ListView) findViewById(R.id.list_detail);
    }

    private void getData(String url)
    {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                       for (int i = 0; i < response.length();i++)
                       {

                           try {
                               JSONObject object = response.getJSONObject(i);
                               list.add(new Song(
                                       object.getInt("Id"),
                                       object.getString("Name"),
                                       object.getString("Singer"),
                                       object.getString("Link"),
                                       object.getString("Image"),
                                       object.getString("Country"),
                                       object.getString("Style")

                               ));

                           } catch (JSONException e) {
                               e.printStackTrace();
                           }
                            adapter.notifyDataSetChanged();
                       }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Main3Activity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(jsonArrayRequest);
    }



}
