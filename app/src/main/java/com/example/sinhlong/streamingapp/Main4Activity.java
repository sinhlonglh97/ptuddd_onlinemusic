package com.example.sinhlong.streamingapp;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main4Activity extends AppCompatActivity {
    //Database database;
    ArrayList<Song> playList = new ArrayList<>();
    ListViewAdapter adapter;
    ListViewOfflineAdapter adapter2;
    String url = "http://192.168.35.108:8080/android/getpl.php";
    ListView  listView;

    List<File> files;
    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                continue;
            } else {
                if(file.getName().endsWith(".mp3")){
                    inFiles.add(file);
                }
            }
        }
        return inFiles;
    }
    ArrayList<OffSong> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        files = getListFiles(new File("/storage/emulated/0/stream_files"));
        String name = getIntent().getStringExtra("name");
        TextView textView = (TextView) findViewById(R.id.txt_ps_pl);
        textView.setText(name);
        if (name.equals("PLAYLIST ONLINE")) {
            getPl(url);
            adapter = new ListViewAdapter(this, playList);
            listView = (ListView) findViewById(R.id.list_ps_pl);
            listView.setAdapter(adapter);
        }
        if(name.equals("DOWNLOAD"))
        {
            getDl();
            adapter2 = new ListViewOfflineAdapter(this,list);
            listView = (ListView) findViewById(R.id.list_ps_pl);
            listView.setAdapter(adapter2);
        }
    }
    private void getDl()
    {

        for(int i = 0 ; i<files.size();i++) {
            String[] tmp = files.get(i).getName().split("\\_");
            String nameoff = tmp[0];
            String [] a = tmp[1].split("\\.");
            String path = "sdcard/stream_files/"+files.get(i).getName();
            list.add(new OffSong(nameoff, a[0], path));
            //Toast.makeText(Main4Activity.this, tmp[1].toString(), Toast.LENGTH_SHORT).show();
        }

    }
    private void getPl(String url)
    {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length();i++)
                        {

                            try {
                                JSONObject object = response.getJSONObject(i);
                                playList.add(new Song(
                                        object.getInt("Id"),
                                        object.getString("Name"),
                                        object.getString("Singer"),
                                        object.getString("Link"),
                                        object.getString("Image"),
                                        object.getString("Country"),
                                        object.getString("Style")

                                ));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Main4Activity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(jsonArrayRequest);
    }
}
