package com.example.sinhlong.streamingapp;

import java.io.Serializable;

public class Song implements Serializable{
    private int id;
    private String name;
    private String singer;
    private String link;
    private String image;
    private String country;
    private String style;


    public Song(int id, String name,String singer, String link, String image,String country,String style) {
        this.id = id;
        //this.imgSinger = imgSinger;
        this.name = name;
        this.singer = singer;
        this.link = link;
        this.image=image;
        this.country=country;
        this.style=style;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

   /* public int getImgSinger() {
        return imgSinger;
    }

    public void setImgSinger(int imgSinger) {
        this.imgSinger = imgSinger;
    }*/

    @Override
    public String toString() {
        return this.name;
    }
}

