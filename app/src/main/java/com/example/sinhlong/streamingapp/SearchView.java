package com.example.sinhlong.streamingapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchView extends AppCompatActivity {
    ListView lvSong;
    ArrayList<Song> list = new ArrayList<>();
    String url = "http://192.168.35.108:8080/android/getdata.php";
    ListViewAdapter lstAdapter;
    EditText edtSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);
        lvSong = (ListView)findViewById(R.id.listDatasong);
        edtSearch = (EditText)findViewById(R.id.edtSearch);
//        list.add(new Song(1,"Nếu em ở lại" , "Khói","http://docs.google.com/uc?export=download&id=1JDaTAHu4hWTwpYRFO-lVh34dmaS6moZD","khoi","VN","RAP"));
//        list.add(new Song(2,"Người âm phủ" , "Osad","http://docs.google.com/uc?export=download&id=1UG7Dr1QYGOCsE-BRXBDr83RU5r31v3zD","osad","VN","RAP"));
//        list.add(new Song(3,"Alone" , "Alanwalker","http://docs.google.com/uc?export=download&id=1-epxN6ih07jNcMiL7IakIV_Z3VcouAiN","alan","VN","RAP"));
        getPl(url);
        lstAdapter = new ListViewAdapter(SearchView.this,list);
        lvSong.setAdapter(lstAdapter);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                lstAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
    private void getPl(String url)
    {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length();i++)
                        {

                            try {
                                JSONObject object = response.getJSONObject(i);
                                list.add(new Song(
                                        object.getInt("Id"),
                                        object.getString("Name"),
                                        object.getString("Singer"),
                                        object.getString("Link"),
                                        object.getString("Image"),
                                        object.getString("Country"),
                                        object.getString("Style")

                                ));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            lstAdapter.notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SearchView.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(jsonArrayRequest);
    }
}
