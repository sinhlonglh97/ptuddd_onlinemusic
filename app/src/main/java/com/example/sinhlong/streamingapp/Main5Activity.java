package com.example.sinhlong.streamingapp;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Main5Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);


        Button btn = (Button) findViewById(R.id.btnThem);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogThem();
            }
        });
    }
    public void DialogThem()
    {
        final Dialog dialog = new Dialog(Main5Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.diglog_add_song);

        final EditText editName = (EditText) dialog.findViewById(R.id.editName);
        final EditText editSinger = (EditText) dialog.findViewById(R.id.editSinger);
        final EditText editLink = (EditText) dialog.findViewById(R.id.editLink);
        final EditText editImage = (EditText) dialog.findViewById(R.id.editImage);
        final EditText editCountry = (EditText) dialog.findViewById(R.id.editCountry);
        final EditText editStyle = (EditText) dialog.findViewById(R.id.editStyle);


        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = ( Button) dialog.findViewById(R.id.btnCancel);


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://192.168.35.108:8080/android/insertdata.php";
                RequestQueue requestQueue = Volley.newRequestQueue(Main5Activity.this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("success")) {
                            Toast.makeText(Main5Activity.this, "Thêm Thành Công", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        } else{
                            Toast.makeText(Main5Activity.this, "Lỗi thêm", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Main5Activity.this, "Xảy ra lỗi", Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("Name",editName.getText().toString());
                        params.put("Singer",editSinger.getText().toString());
                        params.put("Link",editLink.getText().toString());
                        params.put("Image",editImage.getText().toString());
                        params.put("Country",editCountry.getText().toString());
                        params.put("Style",editStyle.getText().toString());

                        return params;
                    }
                };
                requestQueue.add(stringRequest);

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
