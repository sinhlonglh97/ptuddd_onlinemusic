package com.example.sinhlong.streamingapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter {
    private Context context;
    private int[] image;

    public GridViewAdapter(Context context,  int[] image) {
        this.context = context;

        this.image = image;
    }


    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.gridview_item_layout,null);

        ImageView imgView = (ImageView) view.findViewById(R.id.imgViewItem);

        imgView.setImageResource(image[i]);
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent=new Intent(context, Main3Activity.class);

                //Song song = list.get(position);
                //ArrayList<Integer> l = new ArrayList<>();
               // l.add(i);
                Intent vitri = myIntent.putExtra("vitri",i);
                Intent hinhanh = myIntent.putExtra("hinhanh",image[i]);
                context.startActivity(myIntent);
            }
        });
        return view;
    }
}