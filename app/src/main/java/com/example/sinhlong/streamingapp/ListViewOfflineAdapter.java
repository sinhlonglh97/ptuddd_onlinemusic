package com.example.sinhlong.streamingapp;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListViewOfflineAdapter extends BaseAdapter{


    public ListViewOfflineAdapter(Context context, List<OffSong> list) {
        this.context = context;
        this.list = list;
    }

    private Context context;
    private List<OffSong> list;
    int layout;




    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private class ViewHolder
    {

        ImageView menu;
        TextView txtNameSong,txtNameSinger;


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ListViewOfflineAdapter.ViewHolder holder;
        if(convertView == null)
        {
            holder = new ListViewOfflineAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listviewoffline_item_layout,null);
            holder.txtNameSong = (TextView) convertView.findViewById(R.id.txtNameSong);
            holder.txtNameSinger = (TextView) convertView.findViewById(R.id.txtNameSinger);
            holder.menu = (ImageView) convertView.findViewById(R.id.imgMore);
            convertView.setTag(holder);

        }
        else
        {
            holder = (ListViewOfflineAdapter.ViewHolder) convertView.getTag();
        }
        final OffSong song = list.get(position);
        holder.txtNameSong.setText(song.getName());
        holder.txtNameSinger.setText(song.getSinger());


        holder.txtNameSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent=new Intent(context, Main6Activity.class);
                ArrayList<Integer> l = new ArrayList<>();
                l.add(position);
                Intent intent = myIntent.putExtra("data", (Serializable) list);
                Intent vitri = myIntent.putExtra("vitri",l);
                context.startActivity(myIntent);

            }
        });
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context,(ImageView)holder.menu);
                popupMenu.getMenuInflater().inflate(R.menu.menu_offline,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId())
                        {
                            case  R.id.delete:
                                break;
                            case R.id.add_playlist:

                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

        return convertView;
    }


}
