package com.example.sinhlong.streamingapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main7Activity extends AppCompatActivity {
    ArrayList<Song> playList = new ArrayList<>();
    ListViewSAdapter adapter;
    TextView textView;
    ListView listView;
    final String url = "http://192.168.35.111:8080/android/search.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);

        connectView();
        textView.setText("Kết quả tìm kiếm");

        adapter = new ListViewSAdapter(Main7Activity.this,playList);


        getlistSearch(url);
        listView.setAdapter(adapter);

    }
    private void getlistSearch(final String url)
    {
        final String query  = "select * from * where name = "+getIntent().getStringExtra("name");
        final RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
             }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("Query",query);
                return params;
            }


        };
        requestQueue.add(stringRequest);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length();i++)
                        {

                            try {
                                JSONObject object = response.getJSONObject(i);
                                playList.add(new Song(
                                        object.getInt("Id"),
                                        object.getString("Name"),
                                        object.getString("Singer"),
                                        object.getString("Link"),
                                        object.getString("Image"),
                                        object.getString("Country"),
                                        object.getString("Style")

                                ));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Main7Activity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(jsonArrayRequest);


    }

    void connectView()
    {
       textView = (TextView) findViewById(R.id.txt_ps_pl);
       listView = (ListView) findViewById(R.id.list_ps_pl);

    }
}
