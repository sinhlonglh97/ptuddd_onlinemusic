package com.example.sinhlong.streamingapp.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sinhlong.streamingapp.Main5Activity;
import com.example.sinhlong.streamingapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment {


    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);


        Button btn = (Button) view.findViewById(R.id.btnlogin);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogThem();
            }
        });


        return view;
    }
    public void DialogThem()
    {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login_admin);

        final EditText editUser = (EditText) dialog.findViewById(R.id.editUser);
        final EditText editPwd = (EditText) dialog.findViewById(R.id.editPwd);

        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        Button btnCancel = ( Button) dialog.findViewById(R.id.btnCancel);


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = editUser.getText().toString();
                String pwd = editPwd.getText().toString();
                if(user.equals("admin")&&pwd.equals("admin"))
                {
                    Intent intent = new Intent(getContext(), Main5Activity.class);
                    getContext().startActivity(intent);
                    dialog.dismiss();
                }
                else
                {
                    Toast.makeText(getContext(),"Sai tài khoản hoặc mật khẩu!",Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
