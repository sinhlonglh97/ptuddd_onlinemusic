package com.example.sinhlong.streamingapp;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {


    TextView txtName,txtSinger,txtBegin,txtEnd;
    SeekBar skSong;
    ImageView btnPrev,btnNext,btnPlay,btnDown,btnAddList,disc,btnBack;
    MediaPlayer mediaPlayer;

    Animation animation;
    ArrayList<Song> list = new ArrayList<>();
    ArrayList<Integer> l = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        connectView();
        animation = AnimationUtils.loadAnimation(this,R.anim.disc_rotate);

        //Song song = (Song) intent.getSerializableExtra("data");
        //final ArrayList<Song> list = new ArrayList<Song>();
        l = getIntent().getIntegerArrayListExtra("vitri");
        list = (ArrayList<Song>)getIntent().getSerializableExtra("data");

        khoitaoMedia(l.get(0));
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.pause();
                    disc.clearAnimation();
                    btnPlay.setImageResource(R.drawable.ic_play_arrow_white_24dp);
                }else
                {
                    mediaPlayer.start();
                    disc.startAnimation(animation);
                    btnPlay.setImageResource(R.drawable.ic_pause_white_24dp);
                }
            }
        });



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disc.clearAnimation();
                l.set(0,l.get(0)+1);
                if(l.get(0) > list.size()-1)
                {
                    l.set(0,0);
                }

                if(mediaPlayer.isPlaying())
                    mediaPlayer.stop();
                khoitaoMedia(l.get(0));

                mediaPlayer.start();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disc.clearAnimation();
                l.set(0,l.get(0)-1);
                if(l.get(0) < 0)
                {
                    l.set(0,list.size()-1);
                }

                if(mediaPlayer.isPlaying())
                    mediaPlayer.stop();
                khoitaoMedia(l.get(0));

                mediaPlayer.start();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                Intent intent = new Intent(Main2Activity.this,MainActivity.class);
                startActivity(intent);
            }
        });


    }


    void khoitaoMedia(int n)
    {
        txtName.setText(list.get(n).getName());
        txtSinger.setText(list.get(n).getSinger());
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(list.get(n).getLink());

            mediaPlayer.prepareAsync();

            btnNext.setEnabled(false);
            btnPrev.setEnabled(false);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();

                    setTimeEnd();
                    updateTime();
                    btnPlay.setImageResource(R.drawable.ic_pause_white_24dp);
                    btnNext.setEnabled(true);
                    btnPrev.setEnabled(true);
                    disc.startAnimation(animation);
                }

            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        skSong.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
            }
        });
    }

    void setTimeEnd()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
        txtEnd.setText(dateFormat.format(mediaPlayer.getDuration()));

        skSong.setMax(mediaPlayer.getDuration());

    }
    void updateTime()
    {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
                txtBegin.setText(dateFormat.format(mediaPlayer.getCurrentPosition()));
                skSong.setProgress(mediaPlayer.getCurrentPosition());

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        l.set(0,l.get(0)+1);
                        if(l.get(0) > list.size()-1)
                        {
                            l.set(0,0);
                        }

                        if(mediaPlayer.isPlaying())
                            mediaPlayer.stop();
                        khoitaoMedia(l.get(0));

                        mediaPlayer.start();
                    }
                });

                handler.postDelayed(this,500);
            }
        },100);
    }


    /* public void play(String url)
    {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Toast.makeText(MainActivity.this,url,Toast.LENGTH_SHORT).show();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    void connectView()
    {
        txtName =(TextView) findViewById(R.id.txt_namesong_dp);
        txtSinger =(TextView) findViewById(R.id.txt_singer_dp);
        txtBegin =(TextView) findViewById(R.id.beginSb);
        txtEnd =(TextView) findViewById(R.id.endSb);

        btnPlay = (ImageView) findViewById(R.id.btn_play);
        btnPrev = (ImageView) findViewById(R.id.btn_previous);
        btnNext = (ImageView) findViewById(R.id.btn_next);
        btnDown = (ImageView) findViewById(R.id.btn_down);
        btnAddList = (ImageView) findViewById(R.id.btn_addlist);
        btnBack = (ImageView)findViewById(R.id.btn_back);
        disc = (ImageView) findViewById(R.id.disc_music);
        skSong = (SeekBar) findViewById(R.id.seekbar);

    }

}
