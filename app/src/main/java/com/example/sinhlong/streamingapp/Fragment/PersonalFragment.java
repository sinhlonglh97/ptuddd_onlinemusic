package com.example.sinhlong.streamingapp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sinhlong.streamingapp.ListViewPSAdapter;
import com.example.sinhlong.streamingapp.Main4Activity;
import com.example.sinhlong.streamingapp.R;
import com.example.sinhlong.streamingapp.Song;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalFragment extends Fragment {




    public PersonalFragment() {
        // Required empty public constructor
    }
    ArrayList<String> list1 = new ArrayList<>();
    int []Images={R.drawable.pl,R.drawable.song,R.drawable.singer,R.drawable.dl
    };
    ArrayList<String> list2 = new ArrayList<>();
    int []Images2={R.drawable.pl,R.drawable.holiday
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        list1.clear();
        list1.add("PLAYLIST OFFLINE");
        list1.add("BÀI HÁT");
        list1.add("CA SĨ");
        list1.add("DOWNLOAD");
        list2.clear();
        list2.add("PLAYLIST ONLINE");
        list2.add("HOLIDAY");
        View rootView = inflater.inflate(R.layout.fragment_personal, container, false);

        ListViewPSAdapter adapter1 = new ListViewPSAdapter(getContext(),list1,Images);
        ListView listView1 = (ListView) rootView.findViewById(R.id.list_offline);
        listView1.setAdapter(adapter1);

        ListViewPSAdapter adapter2 = new ListViewPSAdapter(getContext(),list2,Images);
        ListView listView2 = (ListView) rootView.findViewById(R.id.list_online);
        listView2.setAdapter(adapter2);




        return rootView;



    }

}