package com.example.sinhlong.streamingapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListViewPSAdapter extends BaseAdapter{
    public ListViewPSAdapter(Context context, List<String> list, int []img) {
        this.context = context;
        this.list = list;
        this.imgItem = img;
    }
    private Context context;
    private List<String> list;
    int layout;
    private int[] imgItem;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder
    {

        ImageView imgItem;
        TextView txtNameItem;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ListViewPSAdapter.ViewHolder holder;
        if(convertView == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_ps_item_layout,null);
            //LayoutInflater inflater = LayoutInflater.from(context.getContext());
            //convertView = inflater.inflate(R.layout.listview_item_layout,parent,false);
            holder.imgItem=(ImageView) convertView.findViewById(R.id.imgItem);
            holder.txtNameItem = (TextView) convertView.findViewById(R.id.txtItem);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ListViewPSAdapter.ViewHolder) convertView.getTag();
        }
        final String item = list.get(position);
        holder.txtNameItem.setText(item);
        holder.imgItem.setImageResource(imgItem[position]);

        holder.txtNameItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,Main4Activity.class);

                Intent name = intent.putExtra("name",holder.txtNameItem.getText().toString());

                context.startActivity(name);

            }
        });

        return convertView;
    }
}
