package com.example.sinhlong.streamingapp;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.sinhlong.streamingapp.Fragment.HomeFragment;
import com.example.sinhlong.streamingapp.Fragment.PersonalFragment;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchAdapter extends BaseAdapter implements Filterable{


    public SearchAdapter(Context context, ArrayList<Song> mSongarraylist/*, int[] imgSinger*/) {
        this.context = context;
        this.list = mSongarraylist;
        this.listSearch = mSongarraylist;
        //this.imgSinger = imgSinger;
    }

    /*public  ListViewAdapter(HomeFragment context,int resource,ArrayList<Song> list,int []img)
        {
            this.context = context;
            this.layout = resource;
            this.list = list;
            this.imgSinger = img;

        }*/
    private Context context;
    private ArrayList<Song> list;
    private ArrayList<Song> listSearch;
    int layout;
    //private int[] imgSinger;



    @Override
    public int getCount() {
        return listSearch.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                ArrayList<Song> songArrayList = new ArrayList<Song>();
                if(list == null){
                    list = new ArrayList<Song>(listSearch);
                }
                if(charSequence == null || charSequence.length() == 0){
                    results.count = list.size();
                    results.values = list;
                } else {
                    charSequence = charSequence.toString().toLowerCase();
                    for(int i = 0; i < list.size();i++){
                        String data = list.get(i).getName();
                        if(data.toLowerCase().startsWith(charSequence.toString())){
                            songArrayList.add(new Song(list.get(i).getId(),list.get(i).getName(),list.get(i).getSinger(),
                                    list.get(i).getLink(),list.get(i).getImage(),list.get(i).getCountry(),list.get(i).getStyle()));
                        }
                    }
                    results.count = songArrayList.size();
                    results.values = songArrayList;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listSearch = (ArrayList<Song>) filterResults.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    private class ViewHolder
    {

        ImageView imgViewSinger,menu;
        TextView txtNameSong,txtNameSinger;


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if(convertView == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_item_layout,null);
            //LayoutInflater inflater = LayoutInflater.from(context.getContext());
            //convertView = inflater.inflate(R.layout.listview_item_layout,parent,false);
            holder.imgViewSinger =(ImageView) convertView.findViewById(R.id.imgViewSinger);
            holder.txtNameSong = (TextView) convertView.findViewById(R.id.txtNameSong);
            holder.txtNameSinger = (TextView) convertView.findViewById(R.id.txtNameSinger);
            holder.menu = (ImageView) convertView.findViewById(R.id.imgMore);
            convertView.setTag(holder);

        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        final Song song = listSearch.get(position);
        holder.txtNameSong.setText(song.getName());
        holder.txtNameSinger.setText(song.getSinger());
        // int resID =

        holder.imgViewSinger.setImageResource(context.getResources().getIdentifier(song.getImage().toString(),"drawable",context.getPackageName()));
        //holder.imgViewSinger.setImageURI(Uri.parse("../res/image/jayki.jpg"));

        holder.txtNameSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String url = song.getLink().toString();
                //play(url);
                //context.toDisplay();
                Intent myIntent=new Intent(context, Main2Activity.class);

                //Song song = list.get(position);
                ArrayList<Integer> l = new ArrayList<>();
                l.add(position);
                Intent intent = myIntent.putExtra("data", (Serializable) list);
                Intent vitri = myIntent.putExtra("vitri",l);

                context.startActivity(myIntent);

            }
        });
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context,(ImageView)holder.menu);
                popupMenu.getMenuInflater().inflate(R.menu.menu_item,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId())
                        {
                            case  R.id.download:
                                file_download(song.getLink().toString(),song.getName().toString()+"_"+song.getSinger().toString(),song.getName().toString()+"_"+song.getSinger().toString()+".mp3");
                                Toast.makeText(context,song.getLink(),Toast.LENGTH_LONG).show();
                                break;
                            case R.id.add_playlist:
                                String url = "http://10.88.51.148:8080/android/insertpl.php";
                                RequestQueue requestQueue = Volley.newRequestQueue(context);

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        if (response.equals("success")) {
                                            Toast.makeText(context, "Đã thêm vào playlist", Toast.LENGTH_SHORT).show();
                                        } else{
                                            Toast.makeText(context, "Lỗi thêm", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(context, "Xảy ra lỗi", Toast.LENGTH_SHORT).show();
                                    }
                                }){
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String,String> params = new HashMap<>();
                                        params.put("Id",song.getId()+"");
                                        params.put("Name",song.getName().toString());
                                        params.put("Singer",song.getSinger().toString());
                                        params.put("Link",song.getLink().toString());
                                        params.put("Image",song.getImage().toString());
                                        params.put("Country",song.getCountry().toString());
                                        params.put("Style",song.getStyle().toString());
                                        return params;
                                    }
                                };
                                requestQueue.add(stringRequest);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

        return convertView;
    }
    public void file_download(String uRl,String Title,String name) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/stream_files");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle(Title)
                .setDescription("Downloading...")
                .setDestinationInExternalPublicDir("/stream_files", name);

        mgr.enqueue(request);

    }

   /* public void play(String url)
    {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // Toast.makeText(MainActivity.this,url,Toast.LENGTH_SHORT).show();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}