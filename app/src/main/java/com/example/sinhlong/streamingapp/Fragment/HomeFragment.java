package com.example.sinhlong.streamingapp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.example.sinhlong.streamingapp.GridViewAdapter;
import com.example.sinhlong.streamingapp.ListViewAdapter;
import com.example.sinhlong.streamingapp.Main2Activity;
import com.example.sinhlong.streamingapp.R;
import com.example.sinhlong.streamingapp.SearchView;
import com.example.sinhlong.streamingapp.Song;

import java.io.Serializable;
import java.util.ArrayList;



/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }
    ViewFlipper viewFlipper;
    Animation out;
    Animation in;
    GridView gridView,gridView2;
    ListView listView;
    EditText edtSearch;
    int gallery_grid_Images[]={R.drawable.image1, R.drawable.image2, R.drawable.image3,
            R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image1,
            R.drawable.image2, R.drawable.image3, R.drawable.image1
    };


    int []zc_grid_Images={R.drawable.zc_vn, R.drawable.zc_us,
            R.drawable.zc_kpop, R.drawable.zc_top100
    };
    int []st_grid_Images= {R.drawable.item11, R.drawable.item21,
            R.drawable.item31, R.drawable.item61
    };

    int []singer_list_Images= new int[]{R.drawable.khoi, R.drawable.osad,R.drawable.theriver
    };

    ArrayList<Song> list = new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        viewFlipper = (ViewFlipper) rootView.findViewById(R.id.viewflip);

        for(int i=0;i<gallery_grid_Images.length;i++)
        {
            //  This will create dynamic image view and add them to ViewFlipper
            setFlipperImage(gallery_grid_Images[i]);

        }
        list.clear();
        list.add(new Song(1,"Nếu em ở lại" , "Khói","http://docs.google.com/uc?export=download&id=1JDaTAHu4hWTwpYRFO-lVh34dmaS6moZD","khoi","VN","RAP"));
        list.add(new Song(2,"Người âm phủ" , "Osad","http://docs.google.com/uc?export=download&id=1UG7Dr1QYGOCsE-BRXBDr83RU5r31v3zD","osad","VN","RAP"));
        list.add(new Song(3,"Lạc trôi" , "MTP","http://docs.google.com/uc?export=download&id=1AIhPTE2kbTuHm-AEFVb1D6dfgb4OG8Wj","mtplt","VN","TT"));
        gridView = (GridView) rootView.findViewById(R.id.gridview);
        gridView2 = (GridView) rootView.findViewById(R.id.gridview2);
        listView = (ListView) rootView.findViewById(R.id.lstViewSong);
        edtSearch = (EditText)rootView.findViewById(R.id.edtSearch);
        GridViewAdapter adapter = new GridViewAdapter(getContext(),zc_grid_Images);
        GridViewAdapter adapter2 = new GridViewAdapter(getContext(),st_grid_Images);

        gridView.setAdapter(adapter);

        gridView2.setAdapter(adapter2);
        //Context a = inflater.getContext();

        ListViewAdapter lstAdapter = new ListViewAdapter(getContext(),list);





        listView.setAdapter(lstAdapter);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), SearchView.class);
                //i.putExtra("songData",(Serializable)list);

                startActivity(i);
            }
        });

       /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                final TextView txtNameSong = (TextView) view.findViewById(R.id.txtNameSong);
                txtNameSong.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            Intent myIntent=new Intent(getActivity(), Main2Activity.class);

                            Song song = list.get(position);

                            myIntent.putExtra("data",song);

                            startActivity(myIntent);

                    }
                });

            }
        });*/

        return rootView;
    }

    private void setFlipperImage(int res) {
        Log.i("Set Filpper Called", res+"");
        ImageView image = new ImageView(getContext());
        image.setBackgroundResource(res);
        in = AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
        out = AnimationUtils.loadAnimation(getContext(),R.anim.fade_out);
        viewFlipper.setInAnimation(in);
        viewFlipper.setOutAnimation(out);
        viewFlipper.addView(image);
        viewFlipper.setFlipInterval(4000);
        viewFlipper.setAutoStart(true);

    }

    public void toDisplay()
    {
        Intent myIntent=new Intent(getActivity(), Main2Activity.class);
        startActivity(myIntent);
    }
}
