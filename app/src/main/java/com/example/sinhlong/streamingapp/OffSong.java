package com.example.sinhlong.streamingapp;

import java.io.Serializable;

public class OffSong implements Serializable {
    private String name;
    private String singer;
    private String link;



    public OffSong(String name,String singer, String link) {

        this.name = name;
        this.singer = singer;
        this.link = link;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

}
